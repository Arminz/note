package com.example.armin.cafebazaarnoteproject.ui.drawingNote;

/**
 * Created by armin on 7/18/17.
 */

public interface DrawingNotePresenter {

    void onAttach(DrawingNoteFragment noteFragment);

    void saveDrawingNoteRequest();

    void fillView();

    long getParentDirectoryId();

    void revertRequest();
}
