package com.example.armin.cafebazaarnoteproject.data.local.entities;

import com.example.armin.cafebazaarnoteproject.data.DataBase;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;
import com.google.gson.Gson;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by armin on 7/17/17.
 */

@Table(database = DataBase.class)
public class Directory extends BaseModel {


    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String name;

    @Column
    long parentDirectoryId;

    public Directory() {
        id = AppConstants.ROOT_DIRECTORY.ID;
    }

    public Directory(String name, long parentDirectoryId) {
        this.name = name;
        this.parentDirectoryId = parentDirectoryId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getParentDirectoryId() {
        return parentDirectoryId;
    }

    public void setParentDirectoryId(long parentDirectoryId) {
        this.parentDirectoryId = parentDirectoryId;
    }

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }


}
