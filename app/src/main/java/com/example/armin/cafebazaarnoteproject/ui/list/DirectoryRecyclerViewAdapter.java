package com.example.armin.cafebazaarnoteproject.ui.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;

import java.util.List;

public class DirectoryRecyclerViewAdapter extends RecyclerView.Adapter<DirectoryRecyclerViewAdapter.ViewHolder> {

    private final List<Directory> directories;

    private final DirectoryInteraction directoryInteraction;

    private final Context context;

    public DirectoryRecyclerViewAdapter(List<Directory> directories, DirectoryInteraction directoryInteraction, Context context) {
        this.directories = directories;
        this.directoryInteraction = directoryInteraction;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.directory_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.directory = directories.get(position);
        if (directories.get(holder.getAdapterPosition()).getName().trim().isEmpty())
            holder.nameText.setText(context.getString(R.string.untitled));
        else
            holder.nameText.setText(directories.get(holder.getAdapterPosition()).getName());


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directoryInteraction.onClick(directories.get(holder.getAdapterPosition()));

            }
        });
        holder.trashImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directoryInteraction.onTrashClick(directories.get(holder.getAdapterPosition()));

            }
        });
        holder.editImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                directoryInteraction.onEditClick(directories.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return directories.size();
    }

    public void removeDirectory(Directory directory) {
        for (int i = 0 ; i < directories.size() ; i ++) {
            Directory thisDirectory = directories.get(i);
            if (thisDirectory.getId() == directory.getId())
            {
                directories.remove(thisDirectory);
                notifyItemRemoved(i);
//                notifyItemRangeChanged(i,directories.size());
                return;
            }
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView nameText;
        public final ImageView trashImageView;
        public final ImageView editImageView;

        public Directory directory;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            nameText = (TextView) view.findViewById(R.id.name_textView);
            trashImageView = (ImageView) view.findViewById(R.id.trash_image_view);
            editImageView = (ImageView) view.findViewById(R.id.edit_image_view);
        }

    }

    public void swap(List<Directory> directories)
    {
        this.directories.clear();
        this.directories.addAll(directories);
        notifyDataSetChanged();
    }
    public void addDirectory(Directory directory)
    {
        this.directories.add(directory);
        notifyItemInserted(directories.size() - 1);
    }
    public void updateDirectoryView(Directory directory) {
        for (int i = 0 ; i < directories.size() ; i ++) {
            Directory thisDirectory = directories.get(i);
            if (thisDirectory.getId() == directory.getId())
            {
                directories.set(i, directory);
                notifyItemChanged(i);
//                notifyItemRangeChanged(i,directories.size());
                return;
            }
        }
    }

    public interface DirectoryInteraction{
        void onClick(Directory directory);
        void onTrashClick(Directory directory);
        void onEditClick(Directory directory);

    }
}
