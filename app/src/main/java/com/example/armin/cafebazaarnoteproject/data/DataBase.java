package com.example.armin.cafebazaarnoteproject.data;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by armin on 7/17/17.
 */

@Database(name = DataBase.NAME,version = DataBase.VERSION)
public class DataBase {

    public static final String NAME = "database";

    public static final int VERSION = 1;
}
