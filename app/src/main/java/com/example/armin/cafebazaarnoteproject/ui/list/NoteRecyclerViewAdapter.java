package com.example.armin.cafebazaarnoteproject.ui.list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;


import java.util.List;

import butterknife.ButterKnife;

public class NoteRecyclerViewAdapter extends RecyclerView.Adapter<NoteRecyclerViewAdapter.ViewHolder> {

    private final List<Note> notes;
    private NoteInteraction noteInteraction;

    private Context context;

    public NoteRecyclerViewAdapter(List<Note> notes,NoteInteraction noteInteraction,Context context) {
        this.notes = notes;
        this.noteInteraction = noteInteraction;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.note_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


        holder.mItem = notes.get(position);
        if (holder.mItem.getType().equals(AppConstants.PASS_KEYS.NOTE_OPEN.WRITE.DRAWING)) {
            holder.imageView.setImageBitmap(holder.mItem.getDraw());
        }
        else{
            holder.imageView.setImageResource(R.mipmap.text_note2);
//            holder.trashImageView.setImage
        }
        holder.imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        if (notes.get(position).getTitle().trim().isEmpty())
            holder.titleText.setText(context.getString(R.string.untitled));
        else
            holder.titleText.setText(notes.get(position).getTitle());


        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteInteraction.onClick(notes.get(holder.getAdapterPosition()));

            }
        });
        holder.trashImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteInteraction.onLongClick(holder.mItem);

            }
        });
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void addNote(Note note) {
        this.notes.add(note);
        notifyItemInserted(notes.size() - 1);
    }

    public void removeNote(Note note) {
        for (int i = 0 ; i < notes.size() ; i ++) {
            Note thisNote = notes.get(i);
            if (thisNote.getId() == note.getId()) {
                notes.remove(thisNote);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, notes.size());
                return;
            }
        }
    }

    public interface NoteInteraction{
        void onClick(Note note);
        void onLongClick(Note note);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView titleText;

        public final ImageView imageView;

        public Note mItem;

        public ImageView trashImageView;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
            mView = view;
            titleText = (TextView) view.findViewById(R.id.name_textView);
            imageView = (ImageView) view.findViewById(R.id.image_view);
            trashImageView = (ImageView) view.findViewById(R.id.trash_image_view);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + titleText.getText() + "'";
        }
    }

    public void swap(List<Note> notes)
    {
        this.notes.clear();
        this.notes.addAll(notes);
        notifyDataSetChanged();
    }
}
