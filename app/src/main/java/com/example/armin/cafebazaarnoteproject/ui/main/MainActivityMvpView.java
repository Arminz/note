package com.example.armin.cafebazaarnoteproject.ui.main;

import android.os.Bundle;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

/**
 * Created by armin on 7/17/17.
 */

public interface MainActivityMvpView {

    void showItemsListDefault(long directoryId);

    void showItemsListWithNodeAdded(Note note);

    void createNewTextNode(Bundle bundle);

    void createNewDrawingNode(Bundle bundle);

    void getOut();
}
