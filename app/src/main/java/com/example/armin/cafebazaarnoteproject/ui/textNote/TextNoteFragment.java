package com.example.armin.cafebazaarnoteproject.ui.textNote;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

/**
 * Created by armin on 7/18/17.
 */

public interface TextNoteFragment {

    void onNoteSaved(Note newNote);

    String getNoteTitle();

    String getNoteText();

    void setTitleTextView(String title);

    void setTextTextView(String text);
}
