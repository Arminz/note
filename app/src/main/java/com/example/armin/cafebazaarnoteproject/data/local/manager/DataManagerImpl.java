package com.example.armin.cafebazaarnoteproject.data.local.manager;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory_Table;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note_Table;
import com.raizlabs.android.dbflow.sql.language.Select;

import java.util.List;
import java.util.concurrent.Callable;

import rx.Observable;

/**
 * Created by armin on 7/17/17.
 */

public class DataManagerImpl implements DataManager {
    @Override
    public Observable<List<Directory>> readDirectoriesByParentDirectory(final long parentDirectoryId) {
        return Observable.fromCallable(new Callable<List<Directory>>() {
            @Override
            public List<Directory> call() throws Exception {
                List<Directory> directoryList1 = new Select()
                        .from(Directory.class)
                        .where(Directory_Table.parentDirectoryId.eq(parentDirectoryId))
                        .orderBy(Directory_Table.id,true)
                        .queryList();
                return directoryList1;
            }
        });
    }

    @Override
    public Directory writeNewDirectory(final Directory newDirectory) {
        newDirectory.save();
        return newDirectory;
    }

    @Override
    public Observable<List<Note>> readNotesByParentDirectory(final long parentDirectoryId) {
        return Observable.fromCallable(new Callable<List<Note>>() {
            @Override
            public List<Note> call() throws Exception {
                List<Note> noteList = new Select().from(Note.class)
                        .where(Note_Table.parentDirectoryId.eq(parentDirectoryId))
                        .orderBy(Note_Table.id,true)
                        .queryList();
                return noteList;

            }
        });
    }

//    public Observable<Note> readNotesByParentDirectory2(final long parentDirectoryId){
//        return Observable.create(new Observable.OnSubscribe<Note>() {
//            @Override
//            public void call(Subscriber<? super Note> subscriber) {
//                Cursor noteCursor = new Select().from(Note.class)
//                        .where(Note_Table.parentDirectoryId.eq(parentDirectoryId))
//                        .query();
//                SQLite
//                for (int i = 0 ; i < )
//                return;
//            }
//        });
//    }

    @Override
    public Note writeNewNote(final Note newNote)
    {
        newNote.save();
        return newNote;
    }

    @Override
    public Directory getDirectoryById(long directoryId) {
        Directory directory = new Select()
                .from(Directory.class)
                .where(Directory_Table.id.eq(directoryId))
                .querySingle();
        return directory;
    }

    @Override
    public Note readNoteById(long aLong) {
        return new Select()
                .from(Note.class)
                .where(Note_Table.id.eq(aLong))
                .querySingle();
    }

    @Override
    public Observable<Boolean> removeDirectory(final Directory directory) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                directory.delete();
//                SQLite.delete()
//                        .from(Note_Table.class)
//                        .where(Note_Table.parentDirectoryId.eq(directory.getId()))
//                        .execute();
                return true;
            }
        });
    }


    @Override
    public void removeNote(Note note) {
        note.delete();
    }
}
