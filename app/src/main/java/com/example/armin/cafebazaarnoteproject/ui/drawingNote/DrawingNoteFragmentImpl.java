package com.example.armin.cafebazaarnoteproject.ui.drawingNote;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManagerImpl;
import com.example.armin.cafebazaarnoteproject.ui.drawingView.DrawingView;
import com.example.armin.cafebazaarnoteproject.ui.main.MainActivity;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;
import com.thebluealliance.spectrum.SpectrumDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by armin on 7/18/17.
 */

public class DrawingNoteFragmentImpl extends Fragment implements DrawingNoteFragment {

    @BindView(R.id.title_editText)
    protected EditText titleEditText;
    @BindView(R.id.seekBar)
    protected SeekBar seekBar;

    private DrawingNotePresenter mPresenter;
    private DrawingView drawingView;
    private Paint paint;

    public static final String TAG = "DRAWING_FRAGMENT_TAG";

    public DrawingNoteFragmentImpl() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataManagerImpl dataManager = new DataManagerImpl();

        mPresenter = new DrawingNotePresenterImpl(dataManager,getArguments());

        mPresenter.onAttach(this);

//        noteId = getArguments().getLong(AppConstants.PASS_KEYS.NOTE_ID);
//
//        parentDirectoryId = getArguments().getLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID)

    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.drawing_note_fragment,container,false);

        ButterKnife.bind(this,view);

        View drawingView = getDrawingView();
        drawingView.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        ((LinearLayout)view.findViewById(R.id.paint_layout)).addView(drawingView);

        mPresenter.fillView();

        initSeekBar();


        return view;


    }

    private void initSeekBar() {
        seekBar.setMax(AppConstants.NOTE.DRAWING.SIZE.MAX - AppConstants.NOTE.DRAWING.SIZE.MIN);
        seekBar.setProgress(AppConstants.NOTE.DRAWING.SIZE.DEFAULT);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress + AppConstants.NOTE.DRAWING.SIZE.MIN;
                paint.setStrokeWidth(progress);


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
//        blackPointImageView.getLayoutParams().width = AppConstants.NOTE.DRAWING.SIZE.DEFAULT/4;
//        blackPointImageView.getLayoutParams().height = AppConstants.NOTE.DRAWING.SIZE.DEFAULT/4;

    }

    @OnClick(R.id.save_button)
    public void saveNoteRequest()
    {
        mPresenter.saveDrawingNoteRequest();
    }


    private View getDrawingView(){

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setDither(true);
        paint.setColor(AppConstants.NOTE.DRAWING.DEFAULT_COLOR);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeCap(Paint.Cap.ROUND);
        paint.setStrokeWidth(AppConstants.NOTE.DRAWING.SIZE.DEFAULT);
        paint.setXfermode(new PorterDuffXfermode(AppConstants.NOTE.DRAWING.PORTER_TYPE));


        drawingView = new DrawingView(getContext(), paint);

        return drawingView;

    }
    public static Fragment newInstance(Bundle bundle) {
        DrawingNoteFragmentImpl fragment = new DrawingNoteFragmentImpl();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onNoteSaved(Note newNote) {
//        ((MainActivity)getActivity()).showItemsListWithNodeAdded(newNote);
        ((MainActivity)getActivity()).showItemsListDefault(getParentDirectoryId());
    }

    @Override
    public String getNoteTitle() {
        return titleEditText.getText().toString().trim();
    }

    @Override
    public void setTitleTextView(String title) {
        titleEditText.setText(title);
    }

    @Override
    public Bitmap getNoteBitmap() {
        return drawingView.getBitmap();

    }

    @Override
    public void setNoteBitmap(Bitmap bitmap) {
        drawingView.setMBitmap(bitmap);

    }

    @Override
    public void setNoteColor(int color) {
        paint.setColor(color);
    }

    @Override
    public void recycle() {
        drawingView.erase();

    }

    @OnClick(R.id.eraser_button)
    public void clearButton(){
        paint.setColor(Color.TRANSPARENT);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        paint.setAntiAlias(true);
    }
    @OnClick(R.id.erase_button)
    public void eraseView(){
        drawingView.erase();
    }

    @OnClick(R.id.revert_button)
    public void revertNote()
    {
        mPresenter.revertRequest();
    }

    @OnClick(R.id.chooseColor_button)
    public void chooseColor()
    {

        SpectrumDialog.Builder bu = new SpectrumDialog.Builder(getContext());
        bu.setColors(R.array.myColors);
        bu.setSelectedColor(paint.getColor());
        bu.setOnColorSelectedListener(new SpectrumDialog.OnColorSelectedListener() {
            @Override
            public void onColorSelected(boolean positiveResult, @ColorInt int color) {
                setNoteColor(color);
                paint.setXfermode(new PorterDuffXfermode(AppConstants.NOTE.DRAWING.PORTER_TYPE));
            }
        });
        bu.build().show(getFragmentManager(),"colorPaletteDialogue");


    }



    public long getParentDirectoryId() {
        return mPresenter.getParentDirectoryId();
    }

}
