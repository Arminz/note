package com.example.armin.cafebazaarnoteproject.data.local.manager;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

import java.util.List;

import rx.Observable;


/**
 * Created by armin on 7/17/17.
 */

public interface DataManager {

    Observable<List<Directory>> readDirectoriesByParentDirectory(long directoryId);

    Observable<List<Note>> readNotesByParentDirectory(long noteId);

    Note writeNewNote(Note newNote);

    Directory writeNewDirectory(Directory newDirectory);

    Directory getDirectoryById(long directoryId);

    Note readNoteById(long aLong);

    Observable<Boolean> removeDirectory(Directory directory);

    void removeNote(Note note);

//    Observable<Note> readNotesByParentDirectory2(long id);
}
