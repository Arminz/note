package com.example.armin.cafebazaarnoteproject.ui.textNote;

import android.os.Bundle;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManager;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;

/**
 * Created by armin on 7/18/17.
 */

public class TextNotePresenterImpl implements TextNotePresenter {

    private TextNoteFragment noteFragment;
    private DataManager dataManager;

    private Note note;

    private Bundle arguments;

    private String htmlString;

    public TextNotePresenterImpl(DataManager dataManager,Bundle arguments) {
        this.dataManager = dataManager;
        this.arguments = arguments;

        String initType = arguments.getString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE);
        if (initType == null)
            throw new RuntimeException("textNoteFragment initType cannot be null");

        if (initType.equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.NEW)) {
            note = new Note();
            note.setParentDirectoryId(arguments.getLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID));
        }
        else if (initType.equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.EXISTING)) {
            note = dataManager.readNoteById(arguments.getLong(AppConstants.PASS_KEYS.NOTE_ID));

        }
    }

    @Override
    public void onAttach(TextNoteFragment noteFragment){
        this.noteFragment = noteFragment;
    }

    @Override
    public void saveNoteRequest() {
        note.setTitle(noteFragment.getNoteTitle());
        note.setText(noteFragment.getNoteText());
        note.setType(AppConstants.PASS_KEYS.NOTE_OPEN.WRITE.TEXT);
        note = dataManager.writeNewNote(note);
        noteFragment.onNoteSaved(note);
    }

    @Override
    public void fillView() {
        noteFragment.setTitleTextView(note.getTitle());
        noteFragment.setTextTextView(note.getText());
    }

    @Override
    public long getParentDirectoryId() {
        return note.getParentDirectoryId();
    }

    @Override
    public void revertRequest() {
        fillView();
    }
}
