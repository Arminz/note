package com.example.armin.cafebazaarnoteproject.ui.list;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManager;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.example.armin.cafebazaarnoteproject.util.AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID;

/**
 * Created by armin on 7/17/17.
 */

public class ItemListPresenterImpl implements ItemListPresenter {

    private DataManager dataManager;

    private Context context;

    private ItemListFragment itemListFragment;

    private Directory directory;


    public ItemListPresenterImpl(DataManager dataManager, Context context, Bundle arguments) {
        this.dataManager = dataManager;
        this.context = context;
        long directoryId = arguments.getLong(PARENT_DIRECTORY_ID);
        this.directory = dataManager.getDirectoryById(directoryId);
        if (directory == null) {
            directory = new Directory();
            dataManager.writeNewDirectory(new Directory());
        }
    }


    @Override
    public void fillItems() {
        fillDirectoryText();
        dataManager.readDirectoriesByParentDirectory(directory.getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Directory>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Directory> directories) {
                        itemListFragment.showDirectoryItems(directories);
                    }
                });
        dataManager.readNotesByParentDirectory(directory.getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Note>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Note> noteList) {
                        itemListFragment.showNoteItems(noteList);

                    }
                });
//        dataManager.readNotesByParentDirectory2(directory.getId()).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Observer<Note>() {
//                    @Override
//                    public void onCompleted() {
//
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//
//                    }
//
//                    @Override
//                    public void onNext(Note note) {
//                        itemListFragment.onNoteAdded(note);
//
//                    }
//                });

    }

    private void fillDirectoryText()
    {
        if (directory.getId() == AppConstants.ROOT_DIRECTORY.ID)
            itemListFragment.setTextDirectory(AppConstants.ROOT_DIRECTORY.NAME);
        else if (directory.getName().isEmpty())
            itemListFragment.setTextDirectory("بی نام");
        else
            itemListFragment.setTextDirectory(directory.getName());
    }

    @Override
    public void fillItemsWithNoteAdded(final long noteId) {
        fillDirectoryText();
        dataManager.readDirectoriesByParentDirectory(directory.getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Directory>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Directory> directories) {
                        itemListFragment.showDirectoryItems(directories);
                    }
                });
        dataManager.readNotesByParentDirectory(directory.getId()).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<Note>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Note> noteList) {
                        List<Note> prevNodes = new ArrayList<>();
                        Note newNote = null;
                        for (int i = 0; i < noteList.size(); i++) {
                            if (noteList.get(i).getId() != noteId)
                                prevNodes.add(noteList.get(i));
                            else
                                newNote = noteList.get(i);
                        }
                        itemListFragment.showNoteItems(prevNodes);
                        if (newNote == null) {
                            throw new RuntimeException("newNote has not been created!");
                        }
                        itemListFragment.onNoteAdded(newNote);

                    }
                });
    }

    @Override
    public void onAttach(ItemListFragment itemListFragment) {
        this.itemListFragment = itemListFragment;
    }

    @Override
    public void makeDirectory() {
        itemListFragment.inputTextDialogue(new ItemListFragmentImpl.TextInputCallback() {
            @Override
            public void onTextConfirmed(String newDirectoryName) {
                Directory newDirectory = dataManager.writeNewDirectory(new Directory(newDirectoryName, directory.getId()));
                itemListFragment.onDirectoryAdded(newDirectory);
            }
        },null);

    }

    @Override
    public void goUpside() {
        if (directory.getId() == AppConstants.ROOT_DIRECTORY.ID) {
            itemListFragment.getOut();
            return;
        }
        long newDirectoryId = directory.getParentDirectoryId();
        directory = dataManager.getDirectoryById(newDirectoryId);
        fillItems();


    }

    @Override
    public void onDirectoryClicked(long directoryId) {
        directory = dataManager.getDirectoryById(directoryId);
        fillItems();
    }

    @Override
    public void newTextNoteRequest() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID, directory.getId());
        bundle.putString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE, AppConstants.PASS_KEYS.NOTE_OPEN.INIT.NEW);
        itemListFragment.openTextNoteFragment(bundle);
    }

    @Override
    public void onNoteClicked(Note note) {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PASS_KEYS.NOTE_ID, note.getId());
        bundle.putString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE, AppConstants.PASS_KEYS.NOTE_OPEN.INIT.EXISTING);
        switch (note.getType()) {
            case AppConstants.PASS_KEYS.NOTE_OPEN.WRITE.TEXT:
                itemListFragment.openTextNoteFragment(bundle);
                break;
            case AppConstants.PASS_KEYS.NOTE_OPEN.WRITE.DRAWING:
                itemListFragment.openDrawingNoteFragment(bundle);
                break;
            default:
                throw new RuntimeException("Note type does not exists.");
        }
    }

    @Override
    public void newDrawingNoteRequest() {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID, directory.getId());
        bundle.putString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE, AppConstants.PASS_KEYS.NOTE_OPEN.INIT.NEW);
        itemListFragment.openDrawingNoteFragment(bundle);
    }

    @Override
    public void onTrashDirectoryClicked(final Directory directory) {
        dataManager.removeDirectory(directory)
            .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
            .subscribe(new Observer<Boolean>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    Log.d("tag", e.getMessage());
                }

                @Override
                public void onNext(Boolean aBoolean) {
                    itemListFragment.onDirectoryRemoved(directory);

                }
            });
    }

    @Override
    public void onNoteLongClicked(Note note) {
        dataManager.removeNote(note);
        itemListFragment.onNoteRemoved(note);
    }

    @Override
    public void onEditDirectoryClicked(final Directory directory) {
        itemListFragment.inputTextDialogue(new ItemListFragmentImpl.TextInputCallback() {
            @Override
            public void onTextConfirmed(String newDirectoryName) {
                directory.setName(newDirectoryName);
                directory.save();
                itemListFragment.onDirectoryEdited(directory);
            }
        },directory.getName());

    }
}
   
