package com.example.armin.cafebazaarnoteproject.data.local.entities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.example.armin.cafebazaarnoteproject.data.DataBase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.NotNull;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.data.Blob;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.ByteArrayOutputStream;

/**
 * Created by armin on 7/17/17.
 */

@Table(database = DataBase.class)
public class Note extends BaseModel{

    private static final String FIELD_ID = "id";
    private static final String FIELD_TITLE = "title";
    private static final String FIELD_TEXT = "text";
    private static final String FIELD_PARENT_DIRECTORY_ID = "parentDirectoryId";
    private static final String FIELD_TYPE = "type";
    private static final String FIELD_DRAW = "draw";

    @Column
    @PrimaryKey(autoincrement = true)
    long id;

    @Column
    String title;

    @Column
    String text;

    @Column
    long parentDirectoryId;

    @Column
    @NotNull
    String type;

    @Column
    Blob draw;

    public Note() {}


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setParentDirectoryId(long parentDirectoryId) {
        this.parentDirectoryId = parentDirectoryId;
    }

    public long getParentDirectoryId() {
        return parentDirectoryId;
    }

    @Override
    public String toString() {
//        Gson gson = new Gson();
//        return gson.toJson(this);
        return "note: " + getType();
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Bitmap getDraw() {
        byte[] imageData = draw.getBlob();
        return BitmapFactory.decodeByteArray(imageData, 0,imageData.length);
    }

    public void setDraw(Bitmap draw) {

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        draw.compress(Bitmap.CompressFormat.PNG,0, stream);
        this.draw = new Blob(stream.toByteArray());

    }

}
