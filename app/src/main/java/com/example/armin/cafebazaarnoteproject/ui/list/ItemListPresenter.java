package com.example.armin.cafebazaarnoteproject.ui.list;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

/**
 * Created by armin on 7/17/17.
 */

public interface ItemListPresenter {

    void fillItems();

    void fillItemsWithNoteAdded(long noteId);

    void onAttach(ItemListFragment itemListFragment);

    void makeDirectory();

    void goUpside();

    void onDirectoryClicked(long directoryId);

    void newTextNoteRequest();

    void onNoteClicked(Note note);

    void newDrawingNoteRequest();

    void onTrashDirectoryClicked(Directory directory);

    void onNoteLongClicked(Note note);

    void onEditDirectoryClicked(Directory directory);
}
