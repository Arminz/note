package com.example.armin.cafebazaarnoteproject.util;

import android.graphics.Color;
import android.graphics.PorterDuff;

/**
 * Created by armin on 7/19/17.
 */

public final class AppConstants {

    public static final class PASS_KEYS{

        public final static String PARENT_DIRECTORY_ID = "PARENT_DIRECTORY_ID";

        public final static String NOTE_ID = "NOTE_ID";

        public static final class NOTE_OPEN{

            public final static class INIT{

                public final static String TYPE = "INIT_TYPE";

                public final static String NEW = "NEW";

                public final static String EXISTING = "EXISTING";

            }

            public static final class WRITE {

                public final static String TEXT = "TEXT";

                public final static String DRAWING = "DRAWING";

            }
        }
        public static final class LIST_OPEN{

            public final static String TYPE = "INIT_TYPE";

            public final static String INIT_LIST = "INIT_LIST";

            public final static String NOTE_ADDED = "NOTE_ADDED";

        }
    }

    public static final class NOTE{

        public static final class DRAWING {

            public final static int DEFAULT_COLOR = Color.BLACK;

            public final static PorterDuff.Mode PORTER_TYPE = PorterDuff.Mode.SRC_OVER;

            public static final class SIZE{

                public final static int DEFAULT = 15;

                public final static int MIN = 10;

                public final static int MAX = 55;


            }

        }

        public static final class TEXT{

            public static final class SIZE {

                public final static int DEFAULT = 2;

                public final static int MIN = 1;

                public final static int MAX = 7;
            }

            public final static int DEFAULT_COLOR = Color.BLACK;



        }
    }

    public class ROOT_DIRECTORY {

        public final static long ID = 1;

        public static final String NAME = "خانه" ;

    }
}
