package com.example.armin.cafebazaarnoteproject.ui.textNote;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManagerImpl;
import com.example.armin.cafebazaarnoteproject.ui.main.MainActivity;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;
import com.thebluealliance.spectrum.SpectrumDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.richeditor.RichEditor;

/**
 * Created by armin on 7/18/17.
 */

public class TextNoteFragmentImpl extends Fragment implements TextNoteFragment {

    @BindView(R.id.title_editText)
    protected EditText titleEditText;
    @BindView(R.id.seekBar)
    protected SeekBar seekBar;
    @BindView(R.id.text_richEditor)
    protected RichEditor textRichEditor;

    private TextNotePresenter mPresenter;

    public static final String TAG = "NOTE_FRAGMENT_TAG";
    private int currentTextColor = Color.BLACK;

    public TextNoteFragmentImpl() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DataManagerImpl dataManager = new DataManagerImpl();

        mPresenter = new TextNotePresenterImpl(dataManager,getArguments());

        mPresenter.onAttach(this);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.text_note_fragment,container,false);
        ButterKnife.bind(this,view);

        initViews();

        mPresenter.fillView();

        return view;


    }

    private void initViews() {
        initSeekBar();
        initRichTextEditor();
    }

    private void initSeekBar() {

        seekBar.setMax(AppConstants.NOTE.TEXT.SIZE.MAX - AppConstants.NOTE.TEXT.SIZE.MIN);
        seekBar.setProgress(AppConstants.NOTE.TEXT.SIZE.DEFAULT);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textRichEditor.setFontSize(AppConstants.NOTE.TEXT.SIZE.MIN + progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }


    private void initRichTextEditor() {
        textRichEditor.setFontSize(AppConstants.NOTE.TEXT.SIZE.DEFAULT);
        textRichEditor.setTextColor(AppConstants.NOTE.TEXT.DEFAULT_COLOR);
        textRichEditor.setAlignRight();
        textRichEditor.setEditorBackgroundColor(android.R.color.transparent);

    }


    @OnClick(R.id.save_button)
    public void saveNoteRequest()
    {
        mPresenter.saveNoteRequest();
    }

    public static Fragment newInstance(Bundle bundle) {
        TextNoteFragmentImpl fragment = new TextNoteFragmentImpl();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onNoteSaved(Note newNote) {
//        ((MainActivity)getActivity()).showItemsListWithNodeAdded(newNote);
        ((MainActivity)getActivity()).showItemsListDefault(getParentDirectoryId());

    }

    @Override
    public String getNoteTitle() {
        return titleEditText.getText().toString().trim();
    }

    @Override
    public String getNoteText() {
//        return textEditText.getText().toString().trim();
        return textRichEditor.getHtml();
    }

    @Override
    public void setTitleTextView(String title) {
        titleEditText.setText(title);
    }

    @Override
    public void setTextTextView(String text) {
//        textEditText.setText(text);
        textRichEditor.setHtml(text);
    }

    @OnClick(R.id.bold_button)
    public void changeBold()
    {
        textRichEditor.setBold();
    }

    @OnClick(R.id.italic_button)
    public void changeItalic()
    {
        textRichEditor.setItalic();
    }

    @OnClick(R.id.rightAlign_button)
    public void rightAlign()
    {
        textRichEditor.setAlignRight();
    }
    @OnClick(R.id.leftAlign_button)
    public void leftAlign()
    {
        textRichEditor.setAlignLeft();
    }
    @OnClick(R.id.centerAlign_button)
    public void centerAlign()
    {
        textRichEditor.setAlignCenter();
    }
    @OnClick(R.id.revert_button)
    public void revertClicked()
    {
        mPresenter.revertRequest();
    }



    public long getParentDirectoryId() {
        return mPresenter.getParentDirectoryId();
    }

    @OnClick(R.id.chooseColor_button)
    public void chooseColor() {

        SpectrumDialog.Builder bu = new SpectrumDialog.Builder(getContext());
        bu.setColors(R.array.myColors);
        bu.setSelectedColor(currentTextColor);
        bu.setOnColorSelectedListener(new SpectrumDialog.OnColorSelectedListener() {
            @Override
            public void onColorSelected(boolean positiveResult, @ColorInt int color) {
                textRichEditor.setTextColor(color);
                currentTextColor = color;
            }
        });
        bu.build().show(getFragmentManager(), "colorPaletteDialogue");
    }

}
