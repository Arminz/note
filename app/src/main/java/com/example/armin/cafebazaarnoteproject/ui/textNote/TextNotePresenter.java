package com.example.armin.cafebazaarnoteproject.ui.textNote;

/**
 * Created by armin on 7/18/17.
 */

public interface TextNotePresenter {

    void onAttach(TextNoteFragment noteFragment);

    void saveNoteRequest();

    void fillView();

    long getParentDirectoryId();

    void revertRequest();
}
