package com.example.armin.cafebazaarnoteproject.ui.list;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManagerImpl;
import com.example.armin.cafebazaarnoteproject.ui.main.MainActivity;

import static com.example.armin.cafebazaarnoteproject.util.AppConstants.PASS_KEYS.LIST_OPEN.*;
import static com.example.armin.cafebazaarnoteproject.util.AppConstants.PASS_KEYS.NOTE_ID;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;



public class ItemListFragmentImpl extends Fragment implements ItemListFragment{

    @BindView(R.id.text_view)
    protected TextView textView;

    public static String TAG = "ITEM_LIST_FRAGMENT_TAG";

//    private OnNoteListFragmentInteractionListener mNoteListener;

//    private OnDirectoryListFragmentInteractionListener mDirectoryListener;

    private ItemListPresenter mPresenter;
    private RecyclerView noteRecyclerView;
    private RecyclerView directoryRecyclerView;
    private List<Directory> directories;
    private List<Note> notes;
    private NoteRecyclerViewAdapter noteAdapter;
    private DirectoryRecyclerViewAdapter directoryAdapter;

    public ItemListFragmentImpl() {
    }

    public static ItemListFragmentImpl newInstance(Bundle bundle) {
        ItemListFragmentImpl fragment = new ItemListFragmentImpl();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        DataManagerImpl dataManager = new DataManagerImpl();

        mPresenter = new ItemListPresenterImpl(dataManager,getContext(),getArguments());
        mPresenter.onAttach(this);


        directories = new ArrayList<>();
        notes = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        ButterKnife.bind(this,view);

        directoryRecyclerView = (RecyclerView) view.findViewById(R.id.directory_recycler_view);

        noteRecyclerView = (RecyclerView) view.findViewById(R.id.note_recycler_view);

        noteAdapter = new NoteRecyclerViewAdapter(notes, new NoteRecyclerViewAdapter.NoteInteraction() {
            @Override
            public void onClick(Note note) {
                mPresenter.onNoteClicked(note);
            }

            @Override
            public void onLongClick(Note note) {
                mPresenter.onNoteLongClicked(note);
            }
        },getContext());
        noteRecyclerView.setAdapter(noteAdapter);
        noteRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        directoryAdapter = new DirectoryRecyclerViewAdapter(directories, new DirectoryRecyclerViewAdapter.DirectoryInteraction() {
            @Override
            public void onClick(Directory directory) {
//                ((MainActivity)getActivity()).showItemsListDefault(parentDirectory.getId());
                mPresenter.onDirectoryClicked(directory.getId());

            }

            @Override
            public void onTrashClick(Directory directory) {
                mPresenter.onTrashDirectoryClicked(directory);
            }

            @Override
            public void onEditClick(Directory directory) {
                mPresenter.onEditDirectoryClicked(directory);
            }
        },getContext());
        directoryRecyclerView.setAdapter(directoryAdapter);
        directoryRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));

        if (getArguments().getString(TYPE) == null)
            throw new RuntimeException("item list fragment must be initialized with type argument");
        if (getArguments().getString(TYPE).equals(INIT_LIST))
            mPresenter.fillItems();
        if (getArguments().getString(TYPE).equals(NOTE_ADDED))
            mPresenter.fillItemsWithNoteAdded(getArguments().getLong(NOTE_ID));



        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onNoteAdded(Note note) {
        noteAdapter.addNote(note);

    }

    @Override
    public void onDirectoryAdded(Directory directory) {
        directoryAdapter.addDirectory(directory);
    }

    @Override
    public void showDirectoryItems(List<Directory> directories) {
        directoryAdapter.swap(directories);
    }

    @Override
    public void showNoteItems(List<Note> notes) {
        noteAdapter.swap(notes);
    }



    @Override
    public void goUpside() {
        mPresenter.goUpside();
    }

    @Override
    public void setTextDirectory(String text) {
        textView.setText(text);
    }

    @Override
    public void getOut() {
        ((MainActivity)getActivity()).getOut();
    }


    @OnClick(R.id.makeTextNote_fab)
    public void openTextNote()
    {
        mPresenter.newTextNoteRequest();
    }

    @OnClick(R.id.makeDrawingNote_fab)
    public void openDrawingNote()
    {
        mPresenter.newDrawingNoteRequest();
    }

    @Override
    public void openTextNoteFragment(Bundle bundle) {
        ((MainActivity) getActivity()).createNewTextNode(bundle);
    }

    @Override
    public void openDrawingNoteFragment(Bundle bundle) {
        ((MainActivity)getActivity()).createNewDrawingNode(bundle);
    }

    @Override
    public void onDirectoryRemoved(Directory directory) {
        directoryAdapter.removeDirectory(directory);
    }

    @Override
    public void onNoteRemoved(Note note) {
        noteAdapter.removeNote(note);
    }

    @Override
    public void inputTextDialogue(final TextInputCallback textInputCallback, @Nullable String defaultText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final EditText input = new EditText(getContext());
        input.setTextDirection(View.TEXT_DIRECTION_ANY_RTL);

        if (defaultText != null)
            input.setText(defaultText);
        builder.setView(input);
        builder.setTitle(getString(R.string.input_directory_name_editText));
        builder.setIcon(R.mipmap.edit_icon);

        builder.setPositiveButton(R.string.input_text_dialogue_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                textInputCallback.onTextConfirmed(input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.input_text_dialogue_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();

    }

    @Override
    public void setTextDirectoryHide() {
        textView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDirectoryEdited(Directory directory) {
        directoryAdapter.updateDirectoryView(directory);

    }

    @OnClick(R.id.makeDirectory_fab)
    public void makeDirectory()
    {
        mPresenter.makeDirectory();
    }

    public interface TextInputCallback
    {
        void onTextConfirmed(String s);
    }



}
