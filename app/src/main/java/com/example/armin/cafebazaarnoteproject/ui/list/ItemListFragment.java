package com.example.armin.cafebazaarnoteproject.ui.list;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

import java.util.List;

/**
 * Created by armin on 7/17/17.
 */

public interface ItemListFragment {

    void onNoteAdded(Note note);

    void onDirectoryAdded(Directory directory);

    void showDirectoryItems(List<Directory> directoryList);

    void showNoteItems(List<Note> noteList);

    void goUpside();

    void setTextDirectory(String text);

    void getOut();

    /**
     *  open text note view
     *
     * @param bundle  include the state of the (initialize new one or open existingOne)
     */
    void openTextNoteFragment(Bundle bundle);

    /**
     *  open drawing note view
     *
     * @param bundle  include the state of the note (initialize new one or open existingOne)
     */
    void openDrawingNoteFragment(Bundle bundle);

    void onDirectoryRemoved(Directory directory);

    void onNoteRemoved(Note note);

    /**
     * open dialogueBox to get text from user
     *
     * @param textInputCallback   dialogueBox submit callback
     * @param defaultText         default text in editText, if null there would be empty editText
     */
    void inputTextDialogue(ItemListFragmentImpl.TextInputCallback textInputCallback, @Nullable String defaultText);

    void setTextDirectoryHide();

    void onDirectoryEdited(Directory directory);
}
