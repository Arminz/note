package com.example.armin.cafebazaarnoteproject.ui.drawingNote;

import android.graphics.Bitmap;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;

/**
 * Created by armin on 7/18/17.
 */

public interface DrawingNoteFragment {

    /**
     *  call noteFragment given note {@link Note} is saved
     *
     *  @param newNote
     */
    void onNoteSaved(Note newNote);

    String getNoteTitle();

    void setTitleTextView(String title);

    Bitmap getNoteBitmap();

    void setNoteBitmap(Bitmap bitmap);

    void setNoteColor(int color);

    /**
     *  delete view completely
     */
    void recycle();



}
