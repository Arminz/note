package com.example.armin.cafebazaarnoteproject.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.armin.cafebazaarnoteproject.R;
import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.ui.drawingNote.DrawingNoteFragmentImpl;
import com.example.armin.cafebazaarnoteproject.ui.list.ItemListFragmentImpl;
import com.example.armin.cafebazaarnoteproject.ui.textNote.TextNoteFragmentImpl;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;

public class MainActivity extends AppCompatActivity implements MainActivityMvpView{

    private String currentFragment;

    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();

        showItemsListDefault(AppConstants.ROOT_DIRECTORY.ID);
    }

    @Override
    public void showItemsListDefault(long parentDirectoryId)
    {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID, parentDirectoryId);
        bundle.putString(AppConstants.PASS_KEYS.LIST_OPEN.TYPE, AppConstants.PASS_KEYS.LIST_OPEN.INIT_LIST);
        showItemsList(bundle);
    }

    @Override
    public void showItemsListWithNodeAdded(Note note) {
        Bundle bundle = new Bundle();
        bundle.putLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID, note.getParentDirectoryId());
        bundle.putString(AppConstants.PASS_KEYS.LIST_OPEN.TYPE, AppConstants.PASS_KEYS.LIST_OPEN.NOTE_ADDED);
        bundle.putLong(AppConstants.PASS_KEYS.NOTE_ID,note.getId());
        showItemsList(bundle);

    }


    private void showItemsList(Bundle bundle)
    {
        Fragment fragment = fragmentManager.findFragmentByTag(ItemListFragmentImpl.TAG);
        if (fragment == null)
            fragment = ItemListFragmentImpl.newInstance(bundle);
        else
            fragment.setArguments(bundle);
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_activity_layout, fragment, ItemListFragmentImpl.TAG)
                .commitNow();
        currentFragment = ItemListFragmentImpl.TAG;
    }

    @Override
    public void createNewTextNode(Bundle bundle)
    {
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_activity_layout, TextNoteFragmentImpl.newInstance(bundle), TextNoteFragmentImpl.TAG)
                .commitNow();
        currentFragment = TextNoteFragmentImpl.TAG;

    }

    @Override
    public void onBackPressed() {
        if (currentFragment.equals(TextNoteFragmentImpl.TAG) ){
            TextNoteFragmentImpl noteFragment = (TextNoteFragmentImpl) fragmentManager.findFragmentByTag(TextNoteFragmentImpl.TAG);
            long directoryId = noteFragment.getParentDirectoryId();
            showItemsListDefault(directoryId);
        }
        else if (currentFragment.equals(DrawingNoteFragmentImpl.TAG))
        {
            DrawingNoteFragmentImpl noteFragment = (DrawingNoteFragmentImpl) fragmentManager.findFragmentByTag(DrawingNoteFragmentImpl.TAG);
            long directoryId = noteFragment.getParentDirectoryId();
            showItemsListDefault(directoryId);

        }
        else if (currentFragment.equals(ItemListFragmentImpl.TAG)) {
            ItemListFragmentImpl itemListFragment = (ItemListFragmentImpl) fragmentManager.findFragmentByTag(ItemListFragmentImpl.TAG);
            itemListFragment.goUpside();
        }
    }

    @Override
    public void getOut() {
        super.onBackPressed();
    }

    @Override
    public void createNewDrawingNode(Bundle bundle) {
        fragmentManager
                .beginTransaction()
                .replace(R.id.main_activity_layout, DrawingNoteFragmentImpl.newInstance(bundle), DrawingNoteFragmentImpl.TAG)
                .commitNow();
        currentFragment = DrawingNoteFragmentImpl.TAG;
    }
}
