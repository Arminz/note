package com.example.armin.cafebazaarnoteproject.ui.drawingNote;

import android.os.Bundle;

import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
import com.example.armin.cafebazaarnoteproject.data.local.manager.DataManager;
import com.example.armin.cafebazaarnoteproject.util.AppConstants;

/**
 * Created by armin on 7/18/17.
 */

public class DrawingNotePresenterImpl implements DrawingNotePresenter {

    private DrawingNoteFragment noteFragment;
    private DataManager dataManager;

    private Note note;

    private Bundle arguments;

    public DrawingNotePresenterImpl(DataManager dataManager, Bundle arguments) {
        this.dataManager = dataManager;

        this.arguments = arguments;

        String initType = arguments.getString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE);
        if (initType == null)
            throw new RuntimeException("textNoteFragment initType cannot be null");

        if (initType.equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.NEW)) {
            note = new Note();
            note.setParentDirectoryId(arguments.getLong(AppConstants.PASS_KEYS.PARENT_DIRECTORY_ID));
        }
        else if (initType.equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.EXISTING)) {
            note = dataManager.readNoteById(arguments.getLong(AppConstants.PASS_KEYS.NOTE_ID));

        }
    }

    @Override
    public void onAttach(DrawingNoteFragment noteFragment){
        this.noteFragment = noteFragment;
    }

    @Override
    public void saveDrawingNoteRequest() {
        note.setTitle(noteFragment.getNoteTitle());
        note.setDraw(noteFragment.getNoteBitmap());
        note.setType(AppConstants.PASS_KEYS.NOTE_OPEN.WRITE.DRAWING);
        note = dataManager.writeNewNote(note);
        noteFragment.onNoteSaved(note);
    }

    @Override
    public void fillView() {
//        if (arguments.getString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE).equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.EXISTING)) {
            noteFragment.setTitleTextView(note.getTitle());

        if (arguments.getString(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.TYPE).equals(AppConstants.PASS_KEYS.NOTE_OPEN.INIT.EXISTING))
            noteFragment.setNoteBitmap(note.getDraw());
        else
            noteFragment.recycle();
//        }


    }

    @Override
    public void revertRequest() {
        fillView();
    }

    @Override
    public long getParentDirectoryId() {
        return note.getParentDirectoryId();
    }
}
