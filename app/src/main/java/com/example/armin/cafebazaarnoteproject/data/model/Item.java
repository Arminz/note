//package com.example.armin.cafebazaarnoteproject.data.model;
//
//import com.example.armin.cafebazaarnoteproject.data.local.entities.Directory;
//import com.example.armin.cafebazaarnoteproject.data.local.entities.Note;
//import com.google.gson.Gson;
//
///**
// * Created by armin on 7/17/17.
// */
//
//public class Item {
//
//    public static String NOTE_TYPE = "NOTE_TYPE";
//
//    public static String DIRECTORY_TYPE = "DIRECTORY";
//
//    private String type;
//
//    private Note note;
//
//    private Directory directory;
//
//    public Item(Directory directory) {
//        this.type = DIRECTORY_TYPE;
//        this.directory = directory;
//        this.note = null;
//    }
//
//    public Item(Note note) {
//        this.type = NOTE_TYPE;
//        this.note = note;
//        this.directory = null;
//    }
//
//    @Override
//    public String toString() {
//        Gson gson = new Gson();
//        return gson.toJson(this);
//    }
//}
